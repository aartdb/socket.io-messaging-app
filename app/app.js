const socket = io('ws://localhost:8080');

const receivedMessagesContainerEl = document.querySelector('.js-received-chats');
socket.on('message', text => {
    const el = document.createElement('div');
    el.innerHTML = text;
    receivedMessagesContainerEl.prepend(el);
})
let name = '';
const nameEl = document.querySelector('.js-name');
const messageToSendEl = document.querySelector('.js-message-to-send');
nameEl.addEventListener('input', (e) => {
    name = e.target.value;
    messageToSendEl.disabled = !name; // only enable if the name has been set
});

document.querySelector('.js-message-form').addEventListener('submit', e => {
    e.preventDefault();

    const message = messageToSendEl.value;
    if(!message) return;

    messageToSendEl.value = '';
    socket.emit('message', {
        name,
        message
    });
})
