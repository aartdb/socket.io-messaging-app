const http = require('http').createServer();
const io = require('socket.io')(http, {
    cors: { origin: '*' } // for this trial, just allow all urls access
})

io.on('connection', socket => {
    console.log('Someone connected');

    socket.on('message', ({name, message}) => {
        io.emit('message', `${name} said ${message}`); // also emit this message to all other users
    })
})

const port = 8080;
http.listen(port, () => console.log(`Listening on http://localhost:${port}...`))